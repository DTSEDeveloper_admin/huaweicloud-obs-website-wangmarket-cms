<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="参数设置"/>
</jsp:include>
<%
String keyId = SystemUtil.get("HUAWEIYUN_ACCESSKEYID");
if(keyId == null || keyId.equals("null")){
	keyId = "";
}

String keySecret = SystemUtil.get("HUAWEIYUN_ACCESSKEYSECRET");
if(keySecret == null || keySecret.equals("null")){
	keySecret = "";
}

%>

<style>
	
	.content{
		width: 600px;
		min-height:80%;
   margin: 0 auto;
   box-shadow: rgba(0, 0, 0, 0.06) 0px 1px 10px 2px;
   padding: 30px;
   margin-top: 50px;
	}
	.title{
		border-bottom: 1px solid #eee;
   padding-top: 20px;
   padding-left: 10px;
   padding-bottom: 20px;
   font-size: 28px;
   margin-bottom: 20px;
   text-align:center;
	}
	.content ul{
		padding-left: 20px;
	}
	.content ul li{
		list-style-type: decimal;
		padding-left:10px;
		padding-bottom:4px;
	}
	.content ul li img{
		max-width:250px;
		padding:4px;
		padding-left:40px;
	}
	.info{
		font-size:14px;
		line-height: 22px;
	}
	.info h2,h3,h4,h5{
	border-bottom: 1px solid #eee;
   padding-top: 23px;
   margin-bottom: 10px;
   padding-bottom: 5px;
	}
	
	@media only screen and (max-width: 700px) {
		.content{
			width:auto;
			margin-top: 0px;
			box-shadow: rgba(0, 0, 0, 0.06) 0px 0px 0px 0px;
		}
		
	}
	
	a{
		color: blue;
	}
</style>

<div class="content">
	<div class="title">
		设置 Access Key Id、Access Key Secret 
	</div>
	
	
	<div class="info">
		
		<table class="layui-table iw_table">
		  <tbody>
			   	<tr>
			   		<td>Access Key Id ：</td>
			   		<td id="HUAWEIYUN_ACCESSKEYID"><%=keyId %></td>
					<td style="width:110px;">
						<botton class="layui-btn layui-btn-sm" onclick="updateValue('请填入 Access Key Id', 'HUAWEIYUN_ACCESSKEYID', '<%=keyId %>');" style="margin-left: 3px;"><i class="layui-icon">&#xe642;</i></botton>
					</td>
				</tr>
				<tr>
			   		<td>Access Key Secret ：</td>
			   		<td id="HUAWEIYUN_ACCESSKEYSECRET"><%=keySecret %></td>
					<td style="width:110px;">
						<botton class="layui-btn layui-btn-sm" onclick="updateValue('请填入 Access Key Secret', 'HUAWEIYUN_ACCESSKEYSECRET', '<%=keySecret %>');" style="margin-left: 3px;"><i class="layui-icon">&#xe642;</i></botton>
					</td>
				</tr>
		  </tbody>
		</table>
		
		<div style="height: 80px;line-height: 1.3rem;text-align: right;padding-right:40px;font-size: 0.6rem;color: gray;padding-top: 1.5rem;">
			<span style="color: red;">注：</span><a href = "getAK.jsp" style="color: blue;" target = "_blank">参数获取方式可 [&nbsp;点此查看&nbsp;]</a>
		</div>
		
	</div>
</div>


<script type="text/javascript">
/**
 * 修改
 * title 显示的标题，传入如 请填入 Access Key Id
 * name system表的name，传入如 HUAWEIYUN_ACCESSKEYID
 * value system 表的value值
 */
function updateValue(title, name, value){
	msg.textarea(title, function(v){
		
		msg.loading('保存中');
		post('variableSave.json', {name:name, value:v}, function(data){
			msg.close();
			if(data.result == '0'){
				msg.alert(data.info);
				return;
			}
			
			msg.info('保存成功',function(){
				window.location.reload();
			});
		});
	}, value);
}

if(document.getElementById('HUAWEIYUN_ACCESSKEYID').innerHTML.length > 10 && document.getElementById('HUAWEIYUN_ACCESSKEYSECRET').innerHTML.length > 10){
	msg.confirm('检测到您已填写完成，是否进入下一步？',function(){
		window.location.href='/plugin/huaWeiYunServiceCreate/set/setArea.do';
	});
}
</script>

<jsp:include page="/wm/common/foot.jsp"></jsp:include> 