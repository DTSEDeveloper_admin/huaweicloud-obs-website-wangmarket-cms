package com.xnx3.wangmarket.serverless.util;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.xnx3.j2ee.util.ApplicationPropertiesUtil;

public class SystemPropertiesUtil {
private static Properties properties;	//application.properties
	
	static{
		new SystemPropertiesUtil();
	}
	public SystemPropertiesUtil() {
		if(properties == null){
			try {
	            Resource resource = new ClassPathResource("/system.properties");//
	            properties = PropertiesLoaderUtils.loadProperties(resource);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}
	}
    
	/**
	 * 获取配置属性
	 * @param key 要获取的配置的名字，如 database.name
	 * @return 获取的配置的值。需要判断是否为null
	 */
    public static String getProperty(String key){
    	if(properties == null){
    		return null;
    	}
    	return properties.getProperty(key);
    }
    
    /**
     * 获取 huaweicloud.regurl 的值。如果每获取到，那默认返回我们自己的注册链接关联
     * @return
     */
    public static String getHuaweicloudRegUrl() {
    	String url = getProperty("huaweicloud.regurl");
    	if(url == null || url.length() < 15) {
    		return "http://huawei.leimingyun.com";
    	}else {
    		return url;
    	}
    }
}
