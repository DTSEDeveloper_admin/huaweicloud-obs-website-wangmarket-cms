package com.xnx3.wangmarket.serverless.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xnx3.BaseVO;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.util.ActionLogUtil;
import com.xnx3.j2ee.util.SpringUtil;
import com.xnx3.version.VersionUtil;
import com.xnx3.version.VersionVO;
import com.xnx3.wangmarket.admin.G;
import com.xnx3.wangmarket.admin.controller.BaseController;
import com.xnx3.wangmarket.plugin.htmlSeparate.vo.HtmlStorageObsVO;

/**
 * 版本相关
 * @author 管雷鸣
 */
@Controller(value="ServerLessVersionController")
@RequestMapping("/plugin/serverless/")
public class VersionController extends BaseController {
	/**
	 * 获取当前系统版本，此需要开发者自行修改，根据当前版本的记录方式返回版本号，如返回 3.7
	 * @return
	 */
	private String getCurrentVersion(){
		return G.VERSION;
	}
	
	/**
	 * 检查当前系统是否有最新版本
	 * @return 若有最新版本，VersionVO.getResult == VersionVO.SUCCESS
	 */
	@RequestMapping("getNewVersion.json")
	@ResponseBody
	public VersionVO getNewVersion(HttpServletRequest request){
		ActionLogUtil.insert(request, "检查当前系统是否有最新版本", "当前版本v"+getCurrentVersion());
		
		String domain = "";	//当前网站的域名
		HtmlStorageObsVO hsVO = ((com.xnx3.wangmarket.plugin.htmlSeparate.controller.SetController)SpringUtil.getBean("HtmlSeparatePluginController")).getHtmlStorageObs(request);
		if(hsVO.getResult() - BaseVO.SUCCESS == 0) {
			domain = hsVO.getObs().getCustomDomain();
		}
		
		return VersionUtil.cloudContrast("http://version.xnx3.com/wangmarketserverless.html?type=serverless&domain="+domain, getCurrentVersion());
	}
	
}
